1.
select min(data) data,sum(amt),user_id from
(select t1.*,sign(amt),
decode(sign(amt),1,1,0,1,-1,0),
sum(decode(sign(amt),1,1,0,1,-1,0)) over(partition by user_id order by data) s
 from t1)
group by user_id,s order by data;

with t1 as(select t.*,
           case when amt>0 then data else '0' end d from t),
           t2 as(select t1.*,
                 max(d) over(partition by user_id order by data) m
                 from t1) 
   select min(data) data,sum(amt) amt,user_id 
from t2 group by user_id,m order by data;

2.
select userid from s03 where months_between(sysdate,birth)/12>18;

3.
delete from names where (name,nid) not in
(select name,min(nid) from names group by name);
方法2：
delete from names a where not exists
(select 1 from
(select name,min(nid) n from names group by name) b where a.name=b.name
and a.nid=b.n);

4.
select * from tt
pivot(max(amt) for quarter in (1 q1,2 q2,3 q3,4 q4));

5.
5.1
1.
select 姓名  from 成绩 group by  姓名 having min(分数)>80;
2.
select  姓名 from
(select 姓名,学科 from
(select  a.*,
avg(分数) over(partition by 学科) m 
from 成绩 a) where  分数>m) group  by 姓名
having  count(1)=
(select count(distinct 学科) from 成绩);
3.
select 姓名,
sum(case when 学科='语文' then 分数 else 0 end) 语文,
sum(case when 学科='数学' then 分数 else 0 end) 数学
from 成绩 group by 姓名 order by  数学;

5.2
1.
select distinct 老师  from 学生 a join 选课 b  on a.学号=b.学号
join 课程 c on b.课号=c.课号 where a.姓名 like 'wang%' order by 老师;
2.
select 课名 from  选课 a join 课程 b on a.课号=b.课号 group by 课名
having count(1)=
(select count(distinct 学号) from 学生);
3.
select 姓名 from 学生 a join 选课 b on a.学号=b.学号 group  by 姓名
having count(1)<4;
4.
select * from 
(select 姓名 from 学生 a join 选课 b on a.学号=b.学号 group  by 姓名
having count(1)<3)
where 学生 not in
(select 学生 from 学生 a join 选课 b  on a.学号=b.学号
join 课程 c on b.课号=c.课号 where 老师  like '刘%');

5.3
1.
select * from t_stu where s_sex='女' and s_age between 18 and 20  or  s_sex is null order by  s_birthday;
2.
select  c_name,s_name,s_sex,s_money||'元' from  t_stu a join t_class b on a.c_id=b.c_id order by  a.c_id,s_name;
3.
select c_name,count(1) c from  t_stu a join t_class b on a.c_id=b.c_id group by c_name having count(1)>=2
order by c desc;
4.
select 
sum(case when s_sex='男' or  s_sex is null then 1 else 0 end ),
sum(case when s_sex='女'  then 1 else 0 end )
from t_stu;

select s_sex,count(nvl(s_sex,'男')) from t_stu group by s_sex;

5.4
select * from team a,team b where a.name<b.name;

6.
select a.*,
sum(s) over(partition by userid order by m) from
(select userid,to_char(visitdate,'yyyy-mm') m,sum(visitcount) s from t 
group by userid,to_char(visitdate,'yyyy-mm')) a;

7.
1.
select to_char(date,'yyyy-mm'),count(order_id),count(distinct user_id),sum(amount)
from stg.order where to_char(date,'yyyy')=2017  group by to_char(date,'yyyy-mm');
2.
select count(1) from
(select user_id from stg.order where to_char(date,'yyyy-mm') ='2017-11'
minus
select user_id from stg.order where to_char(date,'yyyy-mm') <'2017-11'
);

方法2：
select count(distinct user_id) from stg.order a where to_char(date,'yyyy-mm') ='2017-11' and not exists
(select 1 from stg.order b where to_char(b.date,'yyyy-mm') <'2017-11' and a.user_id=b.user_id);

方法3：
select count(user_id) from 
(select user_id from stg.order group by user_id having(min(to_char(date,'yyyy-mm')))='2017-11');

8.
#!/bin/sh
path="/data"
d=(`ls $path`)
for i in ${d[*]}
do
  if test -d  $path/$i
  then
  tar -P -zcf $i.tar.gz /data/$i/*.dat
  fi
done

9.
#!/bin/bash
for i in `ls`
do
if test -f 
then
s=`ls -l $i|awk '{print $5}'`
if [ $s -gt 10 ];then
mv $i /tmp
fi
fi
done

10.
#!/bin/bash
for i in `ls /tmp`
do
if [ -d $i ];then
echo $i>>/tmp/dirlist.txt
fi
done

11.
#!/bin/bash
a=$(ls /data/in|grep 'shuju_.*_20181215.zip'|awk -F "_" '{print $2}')
echo $a