--1.查询男生、女生人数
select ssex,count(1) from student group by ssex;
--2.查询姓“张”的学生名单
select * from student where sname like '张%';
--3.查询姓“刘”的老师的个数
select count(*) from teacher where tname like '刘%';
--4.查询选了课程的学生人数
select count(distinct sno) from sc;
--5.查询有学生不及格的课程，并按课程号从大到小排列
select cno from sc where score<60 order by cno desc;
--6.查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分
select cno,max(score),min(score) from sc group by cno;
--7.查询不同课程平均分从高到低显示，要显示授课老师
select a.cno,s,tname from
(select cno,avg(score) s from sc group by cno)  a join
course b on a.cno=b.cno join teacher c on b.tno=c.tno
order by s desc;
--8.查询所有学生的选课情况（每个学生选了几门课）
select sname,count(cno) from student a left join sc b on a.sno=b.sno
group by sname;
--9.查询“c001” 课程成绩在80 分以上的学生的学号和姓名
select sno,sname from student where sno in
(select sno from sc where cno='c001' and score>80);
--10.查询“c004”课程分数小于60的同学学号和姓名，按分数降序排列
select a.sno,sname from student a join sc b on a.sno=b.sno
and cno='c004' and score<60 order by score desc;
--11.查询课程名称为“SSH”，且分数低于60 的学生姓名和分数
select sname,score from student a join
(select sno,score from sc where cno =
(select cno from course  where cname='SSH') and score<60) b
on a.sno=b.sno;
--12.查询所有同学的学号、姓名、选课数、总成绩
select a.sno,sname,count(cno),sum(nvl(score,0)) from student a left join
sc b on a.sno=b.sno group by a.sno,sname;
--13.查询每门课程被选修的学生数
select cname,count(sno) from course a left join sc b
on a.cno=b.cno group by cname;
--14.查询至少选修两门课程的学生学号和姓名
select sno,sname from student where sno in
(select sno from sc group by sno having count(1)>=2);
--15.查询出只选修了一门课程的学生学号和姓名
select sno,sname from student where sno in
(select sno from sc group by sno having count(1)=1);
--16.查询平均成绩大于85 的同学的学号、姓名和平均成绩
select a.sno,sname,s from
(select sc.*,avg(score) over(partition by sno) s
from sc ) a join student b on a.sno=b.sno and s>85;
--17.查询所有课程成绩小于60 分的同学的学号、姓名
select sname,sno from student where sno in
(select sno from sc group by sno having max(score)<60);
--18.查询任何一门课程成绩在70 分以上的学生的姓名、课程名称和分数
select sname,cname,score from student a join sc b on a.sno=b.sno
join course c on b.cno=c.cno where b.sno not in
(select sno from sc where score<=70);
--19.查询两门以上不及格课程的同学的学号及其平均成绩
select sno,avg(score) from sc where sno in
(select sno from sc  where score<60 group by sno having count(1)>2)
group by sno;
--20.查询各科成绩前三名的记录:(不考虑成绩并列情况)
select * from 
(select sc.*,
row_number() over(partition by cno order by score desc) r
from sc) where r<=3;
--21.查询每门功课成绩最好的前两名(并列 )
select * from 
(select sc.*,
rank() over(partition by cno order by score desc) r
from sc) where r<=2;
--22.查询1998年出生的学生名单 
select * from student where to_char(sysdate,'yyyy')-1998=sage;
--23.查询同名同姓学生名单，并统计同名人数
insert into student values('s011','张三',20,'女');
select sname  from student group by sname having count(1)>=2;
--24.查询不同课程成绩相同的学生的学号、课程号、学生成绩
select a.* from sc a join sc b on a.cno!=b.cno and a.score=b.score;
--25.查询没有学全所有课的同学的学号、姓名
select a.sno,sname from student a left join sc b on a.sno=b.sno
group by a.sno,sname having count(cno)<
(select count(1) from course);
--26.查询全部学生都选修的课程的课程号和课程名
select cno,cname from course where cno in
(select cno from sc group by cno having count(1)=
(select count(1) from student));
--27.查询选修“谌燕”老师所授课程的学生中，每门课程成绩最高的学生姓名及其成绩
select sname,score from student a join
(select sno,score,
max(score) over(partition by cno) m
from sc where cno in
(select cno from course where tno=
(select tno from teacher where tname='谌燕'))) b
on a.sno=b.sno and score=m;
--28.查询没学过“谌燕”老师讲授的任一门课程的学生姓名 
   select sname from student where sno not in
   (select sno from sc where cno in
   (select cno from course where tno=
(select tno from teacher where tname='谌燕')));

select sname from student a where not exists
(select 1 from
(select sno from sc a join course b on a.cno=b.cno join
teacher c on b.tno=c.tno and tname='谌燕') b
where a.sno=b.sno);
       
--29.查询学过“谌燕”老师所教的所有课的同学的学号、姓名
select sno,sname from student where sno in
(select sno from sc where cno in
(select cno from course where tno in
(select tno from teacher where tname='谌燕'))
group by sno
having count(1)=
(select  count(cno) from course where tno=
(select tno from teacher where tname='谌燕'))); 
 
--30.查询学过“c001”并且也学过编号“c002”课程的所有同学的学号、姓名  
select sno,sname from student where sno in
(select sno from sc where cno='c001'
intersect
select sno from sc where cno='c002'); 
--31.查询“c001”课程比“c002”课程成绩高的所有同学的学号、姓名
select  sno,sname from student where sno in
(select a.sno from
(select * from sc where cno='c001') a,
(select * from sc where cno='c002') b where a.sno=b.sno and a.score>b.score);
--32.统计各科成绩,各分数段人数 : 课程ID,课程名称,[100-85],[85-70],[70-60],[ <60]
select a.cno,cname,
sum(case when score <60 then 1 else 0 end) "[<60]",
sum(case when score <70 and score>=60 then 1 else 0 end) "[70-60]",
sum(case when score <85 and score>=70 then 1 else 0 end) "[85-70]",
sum(case when score <=100 and score>=85 then 1 else 0 end) "[100-85]"
from sc a join course b on a.cno=b.cno
group by a.cno,cname;
--33.查询至少有有一门课与学号为“s001”的同学所学相同的同学的学号和姓名 
select sno,sname from student where sno in
(select sno from sc where cno in
(select cno from sc where sno='s001')); 
--34.查询学过学号为“s001”同学所有门课的其他同学学号和姓名
select  sno,sname from student where sno in
(select a.sno from sc a join
(select cno from sc where sno='s001') b
on a.cno=b.cno and a.sno!='s001'
group by a.sno having count(1)=
(select count(1) from sc where sno='s001'));
--35.查询和“s002”号的同学学习的课程完全相同的其他同学学号和姓名 
select  sno,sname from student where sno in
(select a.sno from sc a left join
(select cno from sc where sno='s002') b
on a.cno=b.cno where a.sno!='s002'
group by a.sno having count(1)=
(select count(1) from sc where sno='s002'));                  
--36.按各科平均成绩从低到高和及格率的百分数从高到低顺序
select cno,avg(score) a,
sum(case when score>=60 then 1 else 0 end)/count(1)*100 s
from sc group by cno order by a,s desc;
--37.